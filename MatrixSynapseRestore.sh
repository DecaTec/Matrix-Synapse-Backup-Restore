#!/bin/bash

#
# Bash script for restoring backups of Matrix Synapse.
#
# Version 1.0.3
#
# Usage:
#   - With backup directory specified in the script: ./MatrixSynapseRestore.sh <BackupName> (e.g. ./MatrixSynapseRestore.sh 20170910_132703)
#   - With backup directory specified by parameter: ./MatrixSynapseRestore.sh <BackupName> <BackupDirectory> (e.g. ./MatrixSynapseRestore.sh 20170910_132703 /media/hdd/matrix_backup)
#
# Requirements:
#	- pigz (https://zlib.net/pigz/) for using backup compression. If not available, you can use another compression algorithm (e.g. gzip)
#
# IMPORTANT
# You have to customize this script (directories, DB, etc.) for your actual environment.
# All entries which need to be customized are tagged with "TODO".
#

# Make sure the script exits when any command fails
set -Eeuo pipefail

# Variables
restore=${1:-} 
backupMainDir=${2:-} 

if [ -z "$backupMainDir" ]; then
	# TODO: The directory where you store the backups (when not specified by args)
    backupMainDir='/media/hdd/matrix_backup'
fi

echo "Backup directory: $backupMainDir"

# TODO: Set this to true, if the backup was created with compression enabled, otherwise false.
useCompression=true

# TOOD: The bare tar command for using compression.
# Use 'tar -xmpzf' if you want to use gzip compression.
compressionCommand="tar -I pigz -xmpf"

currentRestoreDir="${backupMainDir}/${restore}"

# TODO: The directory of your Matrix Synapse installation (this is a directory under your web root)
matrixInstallDir='/etc/matrix-synapse'

# TODO: The lib directory of Matrix Synapse
matrixLibDir='/var/lib/matrix-synapse'

# TODO: The user of Matrix Synapse
matrixUser='matrix-synapse'

# TODO: The name of the database system (one of: postgresql, sqlite)
databaseSystem='postgresql'

#########################
# Specific for PostgreSQL
# You do not have to set these values if using SQLite.
#

# TODO: Your Matrix Synapse database name
matrixDatabase='synapse_db'

# TODO: Your Matrix Syapse database user
dbUser='synapse_db_user'

# TODO: The password of the Matrix Synapse database user
dbPassword='mYpAsSw0rd'

fileNameBackupDb='matrix-db.sql'

# Specific for PostgreSQL
#########################

#####################
# Specific for SQLite
# You do not have to set these values if using PostgreSQL.
#
# TODO: Database file
databaseFileSqlite='/var/lib/matrix-synapse/homeserver.db'
# TODO: Backup file database
fileNameBackupDbFile='homeserver.db'

# Specific for SQLite
#####################

# File names for backup files
# If you prefer other file names, you'll also have to change the MatrixSynaoseBackup.sh script.
fileNameBackupInstallDir='matrix-installdir.tar'
fileNameBackupLibDir='matrix-libdir.tar'

if [ "$useCompression" = true ] ; then
    fileNameBackupInstallDir='matrix-installdir.tar.gz'
    fileNameBackupLibDir='matrix-libdir.tar.gz'
fi

# Function for error messages
errorecho() { cat <<< "$@" 1>&2; }

#
# Check if parameter(s) given
#
if [ $# != "1" ] && [ $# != "2" ]
then
    errorecho "ERROR: No backup name to restore given, or wrong number of parameters!"
    errorecho "Usage: MatrixSynapseRestore.sh 'BackupDate' ['BackupDirectory']"
    exit 1
fi

#
# Check for root
#
if [ "$(id -u)" != "0" ]
then
    errorecho "ERROR: This script has to be run as root!"
    exit 1
fi

#
# Check if backup dir exists
#
if [ ! -d "${currentRestoreDir}" ]
then
	errorecho "ERROR: Backup ${restore} not found!"
    exit 1
fi

#
# Check if the commands for restoring the database are available
#
if [ "${databaseSystem,,}" = "postgresql" ] || [ "${databaseSystem,,}" = "pgsql" ]; then
    if ! [ -x "$(command -v psql)" ]; then
		errorecho "ERROR: PostgreSQL not installed (command psql not found)."
		errorecho "ERROR: No restore of database possible!"
        errorecho "Cancel restore"
        exit 1
	fi
fi

#
# Stop Matrix Synapse
#
echo "$(date +"%H:%M:%S"): Stopping Matrix Synapse..."
systemctl stop matrix-synapse
echo "Done"
echo

#
# Delete old Matrix Synapse directories
#

# Install directory
echo "$(date +"%H:%M:%S"): Deleting old Matrx Synapse install directory..."
rm -r "${matrixInstallDir}"
mkdir -p "${matrixInstallDir}"
echo "Done"
echo

# Lib directory
echo "$(date +"%H:%M:%S"): Deleting old Matrix Synapse lib directory..."
rm -r "${matrixLibDir}"
mkdir -p "${matrixLibDir}"
echo "Done"
echo

#
# Restore install and lib directory
#

# Install directory
echo "$(date +"%H:%M:%S"): Restoring Matrix Synapse install directory..."

if [ "$useCompression" = true ] ; then
    `$compressionCommand "${currentRestoreDir}/${fileNameBackupInstallDir}" -C "${matrixInstallDir}"`
else
    tar -xmpf "${currentRestoreDir}/${fileNameBackupInstallDir}" -C "${matrixInstallDir}"
fi

echo "Done"
echo

# Lib directory
echo "$(date +"%H:%M:%S"): Restoring Matrix Synapse lib directory..."

if [ "$useCompression" = true ] ; then
    `$compressionCommand "${currentRestoreDir}/${fileNameBackupLibDir}" -C "${matrixLibDir}"`
else
    tar -xmpf "${currentRestoreDir}/${fileNameBackupLibDir}" -C "${matrixLibDir}"
fi

echo "Done"
echo

# Set permissions
echo "$(date +"%H:%M:%S"): Setting permissions..."
chown -R "${matrixUser}" "${matrixInstallDir}/homeserver.signing.key"
chown -R "${matrixUser}" "${matrixLibDir}"
echo "Done"
echo

#
# Restore database
#
echo "$(date +"%H:%M:%S"): Dropping old Matrix Synapse DB..."

if [ "${databaseSystem,,}" = "postgresql" ]; then
	sudo -u postgres psql -c "DROP DATABASE ${matrixDatabase};"
else
  # Do not delete if DB file is part of the lib dir, as this was restored before.
  if [[ $databaseFileSqlite != $matrixLibDir* ]]
	then
    rm -r "${databaseFileSqlite}"
  fi
fi

echo "Done"
echo

echo "$(date +"%H:%M:%S"): Creating new DB for Matrix Synapse..."

if [ "${databaseSystem,,}" = "postgresql" ] || [ "${databaseSystem,,}" = "pgsql" ]; then
    echo "Creating new DB for Matrix Synapse..."
    sudo -u postgres psql -c "CREATE DATABASE ${matrixDatabase} ENCODING 'UTF8' LC_COLLATE='C' LC_CTYPE='C' template=template0 OWNER ${dbUser};"
    echo "Done"
    echo
fi

echo "$(date +"%H:%M:%S"): Restoring backup DB..."

if [ "${databaseSystem,,}" = "postgresql" ] || [ "${databaseSystem,,}" = "pgsql" ]; then
	sudo -u postgres psql "${matrixDatabase}" < "${currentRestoreDir}/${fileNameBackupDb}"
else
   # Omit restore if DB file is part of the lib dir
	if [[ $databaseFileSqlite != $matrixLibDir* ]]
	then
    	cp "${currentRestoreDir}/${fileNameBackupDbFile}" "${databaseFileSqlite}"        
	fi

    chown -R "${matrixUser}":nogroup "${databaseFileSqlite}"
fi

echo "Done"
echo

#
# Start Matrix Synapse
#
echo "$(date +"%H:%M:%S"): Starting Matrix Synapse..."
systemctl start matrix-synapse
echo "Done"
echo

echo
echo "DONE!"
echo "$(date +"%H:%M:%S"): Backup ${restore} successfully restored."