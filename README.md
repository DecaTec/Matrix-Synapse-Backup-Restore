[![release](https://img.shields.io/badge/dynamic/json.svg?label=release&url=https://codeberg.org/api/v1/repos/DecaTec/Matrix-Synapse-Backup-Restore/releases&query=$[0].tag_name)](https://codeberg.org/DecaTec/Matrix-Synapse-Backup-Restore/releases)
[![MIT License](https://img.shields.io/badge/license-MIT-green)](https://codeberg.org/DecaTec/Matrix-Synapse-Backup-Restore/src/branch/main/LICENSE)
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=jr%40decatec%2ede&lc=US&item_name=Matrix%20Synapse%20Backup%20Restore&no_note=1&no_shipping=1&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted)


# Matrix-Synapse-Backup-Restore

This repository contains two bash scripts for backup/restore of [Matrix Synapse](https://matrix.org/docs/projects/server/synapse).

## General information

For a complete backup of any Matrix Synapse instance, you'll have to backup these items:
- The install directory of Matrix Synapse (usually `/etc/matrix-synapse/`)
- The lib directory of Matrix Synapse (usually `/var/lib/matrix-synapse/`)
- The Matrix Synapse database (PostgreSQL or SQLite)

The scripts take care of these items to backup automatically.

## Requirements

- *pigz* (https://zlib.net/pigz/) when using backup compression. If not available, you can use another compression algorithm (e.g. gzip)

## Important notes about using the scripts

- After cloning or downloading the repository, you'll have to edit the scripts so that they represent your current Matrix Synapse installation (directories, DB, etc.). All values which need to be customized are marked with *TODO* in the script's comments.

## Setup

1. Clone the repository: `git clone https://codeberg.org/DecaTec/Matrix-Synapse-Backup-Restore.git`
2. Set permissions:
    - `chown -R root Matrix-Synapse-Backup-Restore`
    - `cd Matrix-Synapse-Backup-Restore`
    - `chmod 700 *.sh`
3. Modify the scripts `MatrixSynapseBackup.sh` and `MatrixSynapseRestore.sh` so that these fit to your Matrix Synapse instance (see *TODO* in the script's comments)
4. Start using the scripts: See sections *Backup* and *Restore* below

## Backup

In order to create a backup, simply call the script *MatrixSynapseBackup.sh* on your Matrix Synapse machine.
If this script is called without parameter, the backup is saved in a directory with the current time stamp in your main backup directory: As an example, this would be */media/hdd/matrix_backup/20170910_132703*.
The backup script can also be called with a parameter specifiying the main backup directory, e.g. *./MatrixSynapseBackup.sh /media/hdd/matrix_backup*. In this case, the directory specified will be used as main backup directory. 

## Restore

For restore, just call *MatrixSynapseRestore.sh*. This script expects at least one parameter specifying the name of the backup to be restored. In our example, this would be *20170910_132703* (the time stamp of the backup created before). The full command for a restore would be *./MatrixSynapseRestore.sh 20170910_132703*.
You can also specify the main backup directory with a second parameter, e.g. *./MatrixSynapseRestore.sh 20170910_132703 /media/hdd/matrix_backup*.
