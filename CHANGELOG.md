# Changelog

## 1.0.3

### Restore
- Bugfix: On restore, do not delete SQLite DB after restoring lib dir when the DB file is part of the lib dir

## 1.0.2

### Restore
- Bugfix: Compression command on restore

## 1.0.1

### General
- Bugfix: Unbound variable when no parameters are supplied

## 1.0.0

### General
- The scripts now exit when any command fails.
- Defined the command for compression in the "TODO section" of the script for easier customization.
- Added section for setup in readme.
- Document requirement pigz when using compression.

### Backup
- Bugfix: Fixed the double trailing slash for paths containing the variable `backupdir`.

## 0.2.1
- Added timestamps for all steps.

## 0.2.0

- Omit copying of SQLite database file if it is part of the lib directory of Matrix Synapse.

## 0.1.0

- Initial release